﻿using BattleCity.Game;
using BattleCity.WPF.Implementations;
using System.Threading;
using System.Windows;

namespace BattleCity.WPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnStartClick(object sender, RoutedEventArgs e)
        {
            App.VisualHostInstance = Host;

            var game = new BattleCityGame<WpfGraphicsRenderer, AudioPlayer, Logger, FileSystemManager>();
            game.Load();
            game.StartGame();
        }
    }
}