﻿using BattleCity.Engine.Model;
using BattleCity.WPF.Implementations;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace BattleCity.WPF
{
    public class VisualHost : Canvas
    {
        private readonly Dictionary<string, BitmapImage> _imageCache = new Dictionary<string, BitmapImage>();

        private List<Tile> _tiles = new List<Tile>();

        public void SetBackgroundColor(Color color)
        {
            Call(() => Background = new SolidColorBrush(color));
        }

        public void Draw(List<Tile> tiles)
        {
            _tiles = tiles;
            Call(InvalidateVisual);
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            foreach (var tile in _tiles)
            {
                var path = FileSystemManager.ResolvePath(tile.Name);
                var image = CreateImageFromFile(path, tile.Rotate);
                var rect = new Rect(tile.X, tile.Y, tile.Width, tile.Height);
                dc.DrawImage(image, rect);
            }
        }

        private BitmapImage CreateImageFromFile(string filepath, TileRotate rotate)
        {
            BitmapImage image;

            var key = filepath + rotate;
            if (_imageCache.ContainsKey(key))
            {
                image = _imageCache[key];
            }
            else
            {
                image = new BitmapImage(new Uri(filepath));
                image.Rotation = Convert(rotate);
                _imageCache[key] = image;
            }

            return image;
        }

        private void Call(Action action)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, action);
        }

        private static Rotation Convert(TileRotate rotate)
        {
            switch (rotate)
            {
                case TileRotate.ToLeft:
                    return Rotation.Rotate270;
                case TileRotate.UpsideDown:
                    return Rotation.Rotate180;
                case TileRotate.ToRight:
                    return Rotation.Rotate90;
                default:
                    return Rotation.Rotate0;
            }
        }
    }
}