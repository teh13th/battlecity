﻿using BattleCity.Engine.Interfaces;
using System.Diagnostics;

namespace BattleCity.WPF.Implementations
{
    class Logger : Log
    {
        private static Log _log;
        public static Log Log
        {
            get
            {
                if (_log == null)
                {
                    _log = new Logger();
                    _log.Name = "GUI";
                }
                return _log;
            }
        }

        public Logger()
            : base(false)
        {
        }

        protected override void WriteImplementation(string msg)
        {
            Debug.WriteLine(msg);
        }
    }
}