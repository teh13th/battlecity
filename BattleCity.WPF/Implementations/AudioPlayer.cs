﻿using BattleCity.Engine.Interfaces;
using NAudio.Wave;
using NAudio.WindowsMediaFormat;
using System;
using System.IO;

namespace BattleCity.WPF.Implementations
{
    class AudioPlayer : IAudioPlayer
    {
        private readonly DirectSoundOut _player = new DirectSoundOut();
        private WMAFileReader _wmaReader;

        public void PlaySound(string soundName)
        {
            var fullPath = FileSystemManager.ResolvePath(soundName);
            if (!File.Exists(fullPath))
            {
                throw new Exception("Failed to find sound: " + fullPath);
            }

            if (_wmaReader != null)
            {
                _player.Stop();
                _wmaReader.Dispose();
            }

            _wmaReader = new WMAFileReader(fullPath);
            _player.Init(_wmaReader);
            _player.Play();
        }

        public void Dispose()
        {
            if (_wmaReader != null)
            {
                _player.Stop();
                _wmaReader.Dispose();
            }

            _player.Dispose();
        }
    }
}