﻿using BattleCity.Engine.Interfaces;
using BattleCity.Engine.Model;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace BattleCity.WPF.Implementations
{
    class WpfGraphicsRenderer : IGraphicsRenderer
    {
        private readonly VisualHost _visualHost;

        public bool IsDiffDrawSupported
        {
            get
            {
                return false;
            }
        }

        public WpfGraphicsRenderer()
        {
            _visualHost = App.VisualHostInstance;

            if (_visualHost == null)
            {
                throw new Exception("Visual Host is null!");
            }
        }

        public void SetBackgroundColor(Color color)
        {
            _visualHost.SetBackgroundColor(color);
        }

        public void DrawTiles(List<Tile> tiles, bool diff)
        {
            //TODO: support animation
            _visualHost.Draw(tiles);
        }
    }
}