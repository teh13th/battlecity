﻿using BattleCity.Game.Interfaces;
using System;
using System.IO;

namespace BattleCity.WPF.Implementations
{
    class FileSystemManager : IFileSystemManager
    {
        public bool CheckContentFileExists(string subpath)
        {
            var path = ResolvePath(subpath);
            return File.Exists(path);
        }

        public string[] GetContentFiles(string subpath)
        {
            var path = ResolvePath(subpath);
            return Directory.GetFiles(path);
        }

        public string[] ReadAllLines(string filepath)
        {
            return File.ReadAllLines(filepath);
        }

        public static string ResolvePath(string subpath)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", subpath);
        }
    }
}