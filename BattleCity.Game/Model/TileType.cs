﻿namespace BattleCity.Game.Model
{
    internal enum TileType
    {
        Empty,
        Player1,
        Player2,
        Enemy,
        Base,
        Brick,
        Stone,
        Grass,
        Water,
        Ice
    }
}