﻿using BattleCity.Game.Interfaces;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace BattleCity.Game.Model
{
    internal struct Level
    {
        private const int EnemiesCount = 20;

        public const int MapSize = 26;

        private static readonly Regex IntegerRegex = new Regex(@"\d+");

        internal readonly int OrderNumber;
        internal readonly TileType[,] Map;
        internal readonly int[] EnemiesList;

        private Level(int orderNumber, TileType[,] map, int[] enemiesList)
        {
            OrderNumber = orderNumber;
            Map = map;
            EnemiesList = enemiesList;
        }

        internal static Level LoadFromFile(IFileSystemManager fileManager, string filepath)
        {
            var fileName = filepath.Replace('/', '\\').Split('\\').Last();
            var match = IntegerRegex.Match(fileName);
            if (!match.Success)
            {
                throw new Exception("Invalid level-file name: " + fileName);
            }

            var orderNumber = int.Parse(match.Value);

            var lines = fileManager.ReadAllLines(filepath);
            if (lines == null || lines.Length < MapSize + 1)
            {
                throw new Exception("Invalid level-file: " + filepath);
            }

            var map = new TileType[MapSize, MapSize];
            for (var y = 0; y < MapSize; y++)
            {
                var line = lines[y].ToLower();
                if (line.Length < MapSize)
                {
                    throw new Exception("Invalid line in level-file: " + (y + 1));
                }

                for (var x = 0; x < MapSize; x++)
                {
                    switch(line[x])
                    {
                        case '1':
                            map[y, x] = TileType.Player1;
                            break;
                        case '2':
                            map[y, x] = TileType.Player2;
                            break;
                        case 'p':
                            map[y, x] = TileType.Enemy;
                            break;
                        case 'b':
                            map[y, x] = TileType.Base;
                            break;
                        case '#':
                            map[y, x] = TileType.Brick;
                            break;
                        case '@':
                            map[y, x] = TileType.Stone;
                            break;
                        case 'g':
                            map[y, x] = TileType.Grass;
                            break;
                        case '~':
                            map[y, x] = TileType.Water;
                            break;
                        case '$':
                            map[y, x] = TileType.Ice;
                            break;
                        default:
                            map[y, x] = TileType.Empty;
                            break;
                    }
                }
            }

            var enemiesLine = lines.Last();
            if (enemiesLine.Length < EnemiesCount)
            {
                throw new Exception("Invalid enemies line in level-file");
            }

            var enemiesList = enemiesLine.ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();

            return new Level(orderNumber, map, enemiesList);
        }
    }
}