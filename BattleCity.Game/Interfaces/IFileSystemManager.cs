﻿namespace BattleCity.Game.Interfaces
{
    public interface IFileSystemManager
    {
        string[] ReadAllLines(string filepath);
        string[] GetContentFiles(string subpath);
        bool CheckContentFileExists(string subpath);
    }
}