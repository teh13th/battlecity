﻿using BattleCity.Engine;
using BattleCity.Engine.Interfaces;
using BattleCity.Engine.Model;
using BattleCity.Game.Helpers;
using BattleCity.Game.Interfaces;
using BattleCity.Game.Model;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace BattleCity.Game
{
    public class BattleCityGame<TGraphics, TAudio, TLog, TFile> : IDisposable
        where TGraphics : IGraphicsRenderer, new()
        where TAudio : IAudioPlayer, new()
        where TLog : Log, new()
        where TFile : IFileSystemManager, new()
    {
        private const int Fps = 25;

        private readonly GameEngine<TGraphics, TAudio, TLog> _engine = new GameEngine<TGraphics, TAudio, TLog>(Fps);
        private readonly IFileSystemManager _fileManager = new TFile();
        private readonly Log _log = new TLog();

        private readonly List<Level> _levels = new List<Level>();

        private Queue<Level> _levelsQueue;
        private Level _currentLevel;
        private bool _isLoaded;

        public BattleCityGame()
        {
            _log.Name = "Game";
        }

        public void Load()
        {
            if (_isLoaded)
            {
                _log.Error("Call Load for already loaded game.");
                return;
            }

            LoadLevels();

            CheckRequiredContentExistance();

            _isLoaded = true;

            _engine.Start();
        }

        public void StartGame()
        {
            DrawMap(@"Sounds\intro.wma");
        }

        public void Dispose()
        {
            _engine.Dispose();
        }

        private void LoadLevels()
        {
            _levels.Clear();

            var levelFiles = _fileManager.GetContentFiles("Levels");
            if (levelFiles.Length <= 0)
            {
                throw new Exception("There are no level-files.");
            }

            foreach (var levelFile in levelFiles)
            {
                var level = Level.LoadFromFile(_fileManager, levelFile);
                _levels.Add(level);
            }

            _levels.Sort((x, y) => x.OrderNumber.CompareTo(y.OrderNumber));

            _levelsQueue = new Queue<Level>(_levels);

            _currentLevel = _levelsQueue.Dequeue();
        }

        private void CheckRequiredContentExistance()
        {
            foreach (var contentFile in TileParams.GetAllTileFiles())
            {
                if (!_fileManager.CheckContentFileExists(contentFile))
                {
                    throw new Exception("Required content file is missed: " + contentFile);
                }
            }
        }

        private void DrawMap(string soundName = null)
        {
            _engine.FillScreen(Colors.Black);

            var tiles = new List<Tile>();

            for (var y = 0; y < Level.MapSize; y++)
            {
                for (var x = 0; x < Level.MapSize; x++)
                {
                    var tileType = _currentLevel.Map[y, x];
                    var tileContent = TileParams.GetTileParamsFor(tileType);
                    if (tileContent == null)
                        continue;

                    var tile = new Tile(tileContent.Item1, x * TileParams.SmallTileSize, y * TileParams.SmallTileSize, tileContent.Item2, tileContent.Item2);
                    tiles.Add(tile);
                }
            }

            _engine.PlayScene(new Scene(tiles, soundName));
        }
    }
}