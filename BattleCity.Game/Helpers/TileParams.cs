﻿using BattleCity.Game.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleCity.Game.Helpers
{
    internal static class TileParams
    {
        private const int TileSize = SmallTileSize * 2;

        internal const int SmallTileSize = 16;

        private static readonly Dictionary<TileType, Tuple<string, int>> _contentMap = new Dictionary<TileType, Tuple<string, int>>
        {
            {TileType.Base,     Tuple.Create(@"Images\base.png", TileSize)},
            {TileType.Brick,    Tuple.Create(@"Images\Land\wall.png", SmallTileSize)},
            {TileType.Grass,    Tuple.Create(@"Images\Land\grass.png", SmallTileSize)},
            {TileType.Ice,      Tuple.Create(@"Images\Land\ice.png", SmallTileSize)},
            {TileType.Stone,    Tuple.Create(@"Images\Land\stone.png", SmallTileSize)},
            {TileType.Water,    Tuple.Create(@"Images\Land\water.png", SmallTileSize)}
        };

        internal static Tuple<string, int> GetTileParamsFor(TileType type)
        {
            return _contentMap.ContainsKey(type) ? _contentMap[type] : null;
        }

        internal static IEnumerable<string> GetAllTileFiles()
        {
            return _contentMap.Values.Select(v => v.Item1);
        }
    }
}