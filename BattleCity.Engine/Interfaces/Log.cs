﻿using System;
using System.Threading;

namespace BattleCity.Engine.Interfaces
{
    public abstract class Log
    {
        private const string ErrorType = "ERR";
        private const string DebugType = "DBG";

        private static readonly object SyncObject = new object();

        private readonly bool _logOnlyErrors;

        public string Name { get; set; }

        protected Log(bool logOnlyErrors)
        {
            _logOnlyErrors = logOnlyErrors;
        }

        public void Error(Exception e)
        {
            if (e != null)
            {
                Error(e.ToString());
            }
        }

        public void Error(string msg, params object[] args)
        {
            WriteInternal(msg, ErrorType, args);
        }

        public void Write(string msg, params object[] args)
        {
            if (!_logOnlyErrors)
            {
                WriteInternal(msg, DebugType, args);
            }
        }

        protected abstract void WriteImplementation(string msg);

        private void WriteInternal(string msg, string type, params object[] args)
        {
            if (msg == null)
            {
                return;
            }

            if (args != null && args.Length > 0)
            {
                msg = string.Format(msg, args);
            }

            lock (SyncObject)
            {
                WriteImplementation(string.Format("[{0:dd.MM.yyyy HH:mm:ss.fff}][{1:000}][{2}][{3}] {4}", DateTime.Now, Thread.CurrentThread.ManagedThreadId, type, Name ?? "Main", msg));
            }
        }
    }
}