﻿using BattleCity.Engine.Model;
using System.Collections.Generic;
using System.Windows.Media;

namespace BattleCity.Engine.Interfaces
{
    public interface IGraphicsRenderer
    {
        bool IsDiffDrawSupported { get; }

        void SetBackgroundColor(Color color);
        void DrawTiles(List<Tile> tiles, bool diff);
    }
}