﻿using System;

namespace BattleCity.Engine.Interfaces
{
    public interface IAudioPlayer : IDisposable
    {
        void PlaySound(string soundName);
    }
}