﻿using BattleCity.Engine.Interfaces;
using BattleCity.Engine.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Media;

namespace BattleCity.Engine
{
    public class GameEngine<TGraphics, TAudio, TLog> : IDisposable
        where TGraphics : IGraphicsRenderer, new()
        where TAudio : IAudioPlayer, new()
        where TLog : Log, new()
    {
        private static readonly TimeSpan EngineStopTimeout = TimeSpan.FromSeconds(5);

        private readonly object _syncObject = new object();
        private readonly Queue<Scene> _sceneQueue = new Queue<Scene>();
        private readonly AutoResetEvent _workerFreeEvent = new AutoResetEvent(true);

        private readonly IGraphicsRenderer _renderer = new TGraphics();
        private readonly IAudioPlayer _player = new TAudio();
        private readonly Log _log = new TLog();

        private readonly TimeSpan _renderInterval;

        private Timer _timer;

        public GameEngine(double fps)
        {
            if (fps <= 0)
            {
                throw new Exception("Invalid FPS value: " + fps);
            }

            _log.Name = "Engine";

            _renderInterval = TimeSpan.FromSeconds(1.0 / fps);
            _log.Write("FPS: {0}.", fps);
            _log.Write("Render interval: {0} ms.", _renderInterval.TotalMilliseconds);
        }

        public void Start()
        {
            _log.Write("Start...");
            _timer = new Timer(PlayNextScene, null, TimeSpan.Zero, _renderInterval);
        }

        public void Stop()
        {
            _log.Write("Stop...");

            if (_timer == null)
            {
                _log.Error("Call Stop without Start call.");
                return;
            }

            _timer.Change(Timeout.Infinite, Timeout.Infinite);

            var result = _workerFreeEvent.WaitOne(EngineStopTimeout);
            if (!result)
            {
                throw new Exception("Failed to stop engine!");
            }

            _timer.Dispose();
            _timer = null;

            _sceneQueue.Clear();
        }

        public void FillScreen(Color color)
        {
            _log.Write("Fill screen with color {0}.", color);
            _renderer.SetBackgroundColor(color);
        }

        public void PlayScene(Scene scene)
        {
            lock (_syncObject)
            {
                _log.Write("Add scene \"{0}\" to render queue.", scene);
                _sceneQueue.Enqueue(scene);
            }
        }

        public void Dispose()
        {
            _log.Write("Dispose...");

            try
            {
                if (_timer != null)
                {
                    _timer.Dispose();
                }

                _workerFreeEvent.Dispose();
                _player.Dispose();
            }
            catch (Exception e)
            {
                _log.Error(e);
            }
        }

        private void PlayNextScene(object state)
        {
            if (!_workerFreeEvent.WaitOne(0))
            {
                _log.Write("Frame lost.");
                return;
            }

            try
            {
                Scene sceneToPlay;
                lock (_syncObject)
                {
                    if (_sceneQueue.Count <= 0)
                    {
                        //_log.Write("No scenes to play.");
                        return;
                    }

                    sceneToPlay = _sceneQueue.Dequeue();
                }

                if (sceneToPlay.Tiles.Count <= 0 && sceneToPlay.SoundName == null)
                {
                    _log.Error("Skip empty scene \"{0}\".");
                }
                else
                {
                    if (sceneToPlay.SoundName != null)
                    {
                        _log.Write("Play sound \"{0}\".", sceneToPlay.SoundName);
                        _player.PlaySound(sceneToPlay.SoundName);
                    }

                    if (sceneToPlay.Tiles.Count > 0)
                    {
                        if (_renderer.IsDiffDrawSupported)
                        {
                            _log.Write("Render screen diff (\"{0}\").", sceneToPlay);
                            //TODO: do tiles diff
                            _renderer.DrawTiles(sceneToPlay.Tiles, true);
                        }
                        else
                        {
                            _log.Write("Render screen full (\"{0}\").", sceneToPlay);
                            _renderer.DrawTiles(sceneToPlay.Tiles, false);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _log.Error(e);
            }
            finally
            {
                _workerFreeEvent.Set();
            }
        }
    }
}