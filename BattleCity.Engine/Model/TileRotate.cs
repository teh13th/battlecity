﻿namespace BattleCity.Engine.Model
{
    public enum TileRotate
    {
        None,
        ToRight,
        UpsideDown,
        ToLeft
    }
}