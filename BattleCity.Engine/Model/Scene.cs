﻿using System;
using System.Collections.Generic;

namespace BattleCity.Engine.Model
{
    public struct Scene
    {
        private static readonly Random Rnd = new Random();

        public readonly int Id;
        public readonly List<Tile> Tiles;
        public readonly string SoundName;

        public Scene(List<Tile> tiles, string soundName = null)
        {
            Id = Rnd.Next();
            Tiles = tiles ?? new List<Tile>();
            SoundName = soundName;
        }

        public override string ToString()
        {
            return string.Format("{0} ({1} tiles)", Id, Tiles.Count);
        }
    }
}