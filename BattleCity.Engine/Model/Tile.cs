﻿using System;

namespace BattleCity.Engine.Model
{
    public struct Tile
    {
        public readonly string Name;
        public readonly int X;
        public readonly int Y;
        public readonly int Width;
        public readonly int Height;
        public readonly TileRotate Rotate;
        public readonly bool IsAnimated;

        public Tile(string name, int x, int y, int width = 1, int height = 1, TileRotate rotate = TileRotate.None, bool isAnimated = false)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Empty tile name.");
            }

            if (x < 0)
            {
                throw new Exception("Invalid X coordinate value: " + x);
            }

            if (y < 0)
            {
                throw new Exception("Invalid Y coordinate value: " + y);
            }

            if (width < 0)
            {
                throw new Exception("Invalid width value: " + x);
            }

            if (height < 0)
            {
                throw new Exception("Invalid height value: " + x);
            }

            Name = name;
            X = x;
            Y = y;
            Width = width;
            Height = height;
            Rotate = rotate;
            IsAnimated = isAnimated;
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}, {2}, {3:G}, {4})", Name ?? "<null>", X, Y, Rotate, IsAnimated ? "animated" : "static");
        }
    }
}